import React, { useState } from 'react';

import RoomsScreen from './components/RoomsScreen/RoomsScreen';

function App() {
  const [darkTheme, setTheme] = useState(false);

  const themeClass = darkTheme ? 'dark-theme' : '';

  return (
    // eslint-disable-next-line react/jsx-filename-extension
    <div className={`theme ${themeClass}`}>
      <RoomsScreen
        setDarkTheme={() => setTheme(!darkTheme)}
        darkTheme={darkTheme}
      />
    </div>
  );
}

export default App;
