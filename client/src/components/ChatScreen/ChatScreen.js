// import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MessagesArea from 'components/MessagesArea/MessagesArea';
import Users from 'components/Users/Users';
import { Picker } from 'emoji-mart';
import smile from 'images/smile.png';
import moment from 'moment';
import React, { useState, useEffect, useRef } from 'react';
import io from 'socket.io-client';
import 'emoji-mart/css/emoji-mart.css';

const socket = io('http://localhost:4000');

const ChatScreen = ({ roomID, nickname, roomName }) => {
  let userTyping = false;
  let timeout;
  const textFieldRef = useRef();
  const [users, setUsers] = useState([]);
  const [messages, setMessages] = useState([]);
  const [userIsTyping, setUserIsTyping] = useState('');
  const [messageText, setMessageText] = useState('');
  const [showEmoji, setShowEmoji] = useState(false);

  useEffect(() => {
    socket.emit('iamconnected', roomID, nickname);

    return () => socket.emit('disconnect');
  }, []);

  useEffect(() => {
    socket.on('user-connected', (user) => {
      setUsers([...users, user]);
    });

    socket.on('user-list', (list) => {
      setUsers([...list]);
    });

    socket.on('all-messages', (messages) => {
      setMessages([...messages]);
    });

    socket.on('message-received', (message, direction) => {
      const newDate = new Date();
      const timesent = moment(newDate).format('ddd Do, HH:mm:ss'); // newDate.toISOString()

      setMessages([
        ...messages,
        {
          message,
          direction,
          timesent,
          room: roomID,
        },
      ]);
    });

    socket.on('user-leftroom', (socketID) => {
      setUsers([...users.filter((user) => user.socketID !== socketID)]);
    });

    socket.on('chat-disabled', () => {
      leaveRoom();
    });

    socket.on('useristyping', (message) => {
      setUserIsTyping(message);
    });

    socket.on('userstoppedtyping', (message) => {
      setUserIsTyping(message);
    });
  }, [users, messages]);

  const handleMessageChange = (e) => {
    setMessageText(e.target.value);
  };

  const addEmoji = (e) => {
    const emoji = e.native;

    setMessageText(messageText + emoji);
    setShowEmoji(!showEmoji);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (messageText !== '') {
      const messageTyped = messageText;

      socket.emit('message-added', messageTyped, roomID);
      setMessageText('');
    }
  };

  const keydown = () => {
    userTyping = true;
    if (userTyping) {
      socket.emit('keypress', roomID, socket.id);
    }
  };

  const keyup = () => {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      userTyping = false;
      socket.emit('keyup', roomID, socket.id);
    }, 1500);
  };

  const showEmojiIcons = () => {
    setShowEmoji(!showEmoji);
  };

  const leaveRoom = () => {
    socket.emit('leave-room', socket.id, roomID);
  };

  return (
    <>
      <div className="messages">
        <h3>{roomName}</h3>
        <Button
          variant="contained"
          color="primary"
          className="leave-button"
          onClick={leaveRoom}>
          LEAVE ROOM...
        </Button>
        <div className="flex-2-columns">
          <Users users={users} />
          <MessagesArea messages={messages} />
        </div>
      </div>

      <div className="message-typing-info">{userIsTyping}</div>

      <div
        className="message-form"
        style={{
          position: 'fixed',
          bottom: '4px',
          left: '4px',
          right: '4px',
        }}>
        <form onSubmit={onSubmit}>
          <TextField
            id="outlined-message"
            label="Type your message"
            className="message-text"
            value={messageText}
            onKeyDown={keydown}
            onKeyUp={keyup}
            // eslint-disable-next-line react/no-string-refs
            ref={textFieldRef}
            onChange={handleMessageChange}
          />

          {showEmoji && <Picker onSelect={addEmoji} />}

          <button onClick={showEmojiIcons} type="button">
            <img src={smile} alt="Load emojis" className="emoji_click_button" />
          </button>

          <Button
            variant="contained"
            color="primary"
            className="message-button"
            onClick={onSubmit}>
            SEND
          </Button>
        </form>
      </div>
    </>
  );
};

export default ChatScreen;
