// import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import ChatScreen from 'components/ChatScreen/ChatScreen';
import React, { useState, useEffect } from 'react';

const RoomsScreen = ({ setDarkTheme, darkTheme }) => {
  const [roomsState, setRoomsState] = useState({
    companies: [
      { id: 1, name: 'Room-1' },
      { id: 2, name: 'Room-2' },
      { id: 3, name: 'Room-3' },
    ],
    selectedCompanyRoomId: '',
    selectedCompanyRoomName: '',
    nickname: '',
    showChatScreen: false,
  });

  useEffect(() => {
    document.addEventListener('keydown', enterFunction, false);

    return () => document.removeEventListener('keydown', enterFunction);
  }, [roomsState]);

  const showChat = () => {
    if (roomsState.selectedCompanyRoomId !== '' && roomsState.nickname !== '') {
      setRoomsState({
        ...roomsState,
        showChatScreen: true,
      });
    } else {
      return false;
    }

    return false;
  };

  const enterFunction = (e) => {
    if (e.keyCode === 13) {
      showChat();
    }
  };

  const selectCompanyRoom = (e) => {
    setRoomsState({
      ...roomsState,
      selectedCompanyRoomId: e.target.value,
      selectedCompanyRoomName: e.currentTarget.textContent,
    });
  };

  const handleNickname = (e) => {
    setRoomsState({
      ...roomsState,
      nickname: e.target.value,
    });
  };

  const leaveRoom = () => {
    setRoomsState({
      ...roomsState,
      showChatScreen: false,
      nickname: '',
      selectedCompanyRoomId: '',
      selectedCompanyRoomName: '',
    });
  };

  const selectTheme = (e) => {
    setDarkTheme(e);
  };

  const {
    selectedCompanyRoomId,
    selectedCompanyRoomName,
    showChatScreen,
    nickname,
  } = roomsState;

  return (
    <>
      {!showChatScreen ? (
        <>
          <div className="room-screen">
            <FormControlLabel
              className="toggle-theme-chat"
              control={
                <Checkbox
                  checked={darkTheme}
                  onChange={selectTheme}
                  value="checkedB"
                  color="primary"
                />
              }
              label="Dark theme"
            />
            <FormControl className="form-control">
              <InputLabel htmlFor="age-simple" className="room-label">
                Select company room to join...
              </InputLabel>
              <Select
                value={selectedCompanyRoomId}
                onChange={selectCompanyRoom}
                className="mb-20">
                {roomsState.companies.map((company) => {
                  return (
                    <MenuItem value={company.id} key={company.id}>
                      {company.name}
                    </MenuItem>
                  );
                })}
              </Select>

              <TextField
                id="outlined-nickname"
                label="Type your nickname..."
                className="nickname-text mb-20"
                value={nickname}
                onChange={handleNickname}
              />

              <Button
                variant="contained"
                color="primary"
                className="enter-button"
                onClick={showChat}>
                ENTER
              </Button>
            </FormControl>
          </div>
        </>
      ) : (
        <ChatScreen
          roomID={selectedCompanyRoomId}
          roomName={selectedCompanyRoomName}
          nickname={nickname}
          leaveRoom={leaveRoom}
        />
      )}
    </>
  );
};

export default RoomsScreen;
