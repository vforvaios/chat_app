import React from 'react';

import avatar from '../../images/avatar.png';

const Users = ({ users }) => {
  return (
    <ul className="users-list">
      {users.map((user, key) => {
        return (
          // eslint-disable-next-line react/no-array-index-key
          <li key={key}>
            <img src={avatar} alt="avatar_image" />
            <strong>{user.nickname}</strong>
            <span>just connected!</span>
          </li>
        );
      })}
    </ul>
  );
};

export default Users;
