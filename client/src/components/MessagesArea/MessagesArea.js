import React from 'react';

import avatar from '../../images/avatar.png';

const MessagesArea = ({ messages }) => {
  return (
    <div className="messages-content">
      <ul>
        {messages.map((m, key) => {
          return m.direction === 'left' ? (
            // eslint-disable-next-line react/no-array-index-key
            <li key={key} className={m.direction}>
              <img src={avatar} alt="avatar" />
              <span>{m.message}</span>
              <span>{m.timesent}</span>
            </li>
          ) : (
            // eslint-disable-next-line react/no-array-index-key
            <li key={key} className={m.direction}>
              <span>{m.timesent}</span>
              <span>{m.message}</span>
              <img src={avatar} alt="avatar" />
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default MessagesArea;
