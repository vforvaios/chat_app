const express = require("express");
const socketIO = require("socket.io");
const http = require("http");
const app = express();
const port = 4000;
const server = http.createServer(app);
const io = socketIO(server, {
  cors: {
    origin: "*",
  },
});

let usersConnected = {};
let messages = [];
let i;
let usersOfRoom = [];
let userSockets = [];

/* ON CONNECTION */
io.on("connect", (socket) => {
  userSockets = [];
  usersOfRoom = [];
  usersConnected = {};

  socket.on("iamconnected", function (room, nickname) {
    if (!usersConnected.hasOwnProperty(socket.id.toString())) {
      console.log("User does not exist");
      usersConnected[socket.id.toString()] = {
        socketID: socket.id,
        roomID: room,
        userName: `user_${socket.id}`,
        nickname: nickname,
      };
    } else {
      console.log("User already exists");
      usersConnected[socket.id.toString()].nickname = nickname;
      usersConnected[socket.id.toString()].roomID = room;
    }

    socket.join(room);

    userSockets = [...Object.keys(usersConnected)];

    for (i = 0; i < userSockets.length; i++) {
      if (usersConnected[userSockets[i]].roomID === room) {
        !usersOfRoom.filter((u) => u.socketID === userSockets[i]).length &&
          usersOfRoom.push(usersConnected[userSockets[i]]);
      }
    }

    console.log(usersOfRoom.filter((u) => u.roomID === room));

    socket.emit(
      "user-list",
      usersOfRoom.filter((u) => u.roomID === room)
    );
    socket.to(room).emit("user-connected", {
      socketID: socket.id,
      roomID: room,
      userName: `user_${socket.id}`,
      nickname: nickname,
    });
    socket.emit("all-messages", messages);
  });

  /* LEAVE ROOM */
  socket.on("leave-room", (socketID, roomID) => {
    console.log(`User ${socketID} requesting to leave room ${roomID}`);
    socket.leave(roomID);
    usersConnected[socketID].roomID = "";
    console.log(usersConnected);
    socket.to(roomID).emit("user-leftroom", socketID);
    socket.emit("chat-disabled");
  });

  /* ON DISCONNECT */
  socket.on("disconnect", function () {
    console.log("disconnect: ", socket.id);
    for (var i = 0; i < usersConnected.length; i++) {
      if (usersConnected[i].socketID === socket.id) {
        usersConnected.splice(i, 1);
      }
    }
  });

  /* ON MESSAGE CREATED ON CLIENT */
  socket.on("message-added", (message, room) => {
    socket.emit("message-received", message, "right", room);
    socket.to(room).emit("message-received", message, "left", room);
    messages.push({ message, direction: "left", timesent: "Older message...", room });
  });

  /* USER IS TYPING-INFO */
  socket.on("keypress", (room, socketid) => {
    socket.to(room).emit("useristyping", `${usersConnected[socketid].nickname} is typing...`);
  });

  socket.on("keyup", (room, socketid) => {
    socket.to(room).emit("userstoppedtyping", "");
  });

  //console.log(usersConnected)
});

server.listen(port, () => console.log(`Chat server listening on port ${port}!`));
